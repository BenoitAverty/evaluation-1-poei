# POEI Rennes 3 : Evaluation 1 (Java, POO)

## Livrables et critères d'évaluation

 * Le code source doit être présent sur un projet gitlab dont vous communiquerez l'URL (n'oubliez pas de rendre le dépot public)
 * La partie 2 implique de modifier le code de la partie 1. Pensez à créer un tag ou une branche pour que les deux parties soient clairement idéntifiées (demandez de l'aide si nécéssaire)
 * Pensez aux tests unitaires
 * Les branches autres que master ou celles spécifiées par vous comme contenant du code fini seront ignorées pour la notation. Pensez à utiliser des branches si vous démarrez du travail sans être certain de pouvoir le terminer (demandez de l'aide si nécéssaire).
 
 Pour l'évaluation, les critères généraux suivants seront utilisés : 
 
 * Le programme compile et s'exécute sans erreur (50%)
 * Les classes et méthodes développées correspondent au cahier des charges (30%)
 * Les classes et méthodes développées sont testées avec une méthode main et des tests unitaires (20%) 

## Partie 1 : Développer à partir de spécifications techniques

Cette partie teste les connaissances sur la syntaxe du langage Java (Création de classes, de variables, appels de méthodes...) et la capacité à faire intéragir plusieurs éléments d'un programme.

### Instructions

Le programme est une gestion simple de comptes bancaires. Un compte bancaire appartient à une personne et stocke une quantité d'argent dans une certaine devise. Les opérations possibles sur un compte sont les suivantes : 
 * Déposer de l'argent
 * Retirer de l'argent
 * Consulter le montant actuel sur le compte
 
#### Modélisation

Les instructions suivantes correspondent au diagramme de classes fourni en annexe.

 1. Coder la classe `MonetaryAmount` qui représente une quantité d'argent dans une devise donnée. Une fois créée, une instance de cette classe ne peut pas changer de devise.
    * La quantité d'argent est représentée par un `double` et la devise par une `String`
    * Le constructeur de cette classe doit permettre de spécifier le montant initial et la devise
    * La méthode `addAmount` permet d'ajouter de l'argent
    * La méthode `substractAmount` permet de retirer de l'argent (**le total peut être négatif**)
    * La méthode `toString` permet de convertir une instance de MonetaryAmount en chaine de caractères affichable dans la console.
    * La méthode `getAmount` renvoit la quantité d'argent sous forme d'un nombre à virgule (indépendamment de la devise)
 2. Coder la classe `Account` qui représente un compte bancaire.
    * La quantité d'argent disponible est représentée par un attribut de type `MonetaryAmount` et le nom du propriétaire par une `String`
    * Le constructeur doit permettre de spécifier le nom du propriétaire et la devise du compte. **Initialement, un compte bancaire est vide**. 
    * La méthode `deposit` permet de déposer de l'argent sur le compte
    * La méthode `withdraw` permet de retirer de l'argent sur le compte **Il n'est pas possible de retirer plus d'argent qu'il n'y en a de disponible**. Si on essaie de le faire, la méthode ne fait rien (le montant reste inchangé).
    * La méthode `getCurrentBalance` permet de récupérer le montant actuel sur le compte, sous forme d'une instance de `MonetaryAmount`.
    
#### Code du programme

Les instructions suivantes correspondent au diagramme de séquence fourni en annexe.

écrire un programme qui effectue les actions suivantes : 

 1. Créer un compte bancaire dont le propriétaire est nommé "Obélix". Ce compte stocke de l'argent sous forme de sesterces.
 2. Récupérer le montant disponible sur le compte (méthode `getCurrentBalance` et l'afficher dans la console (il faudra le convertir en String)
 3. Déposer 500.5 sesterces sur le compte en appelant la méthode `deposit`.
 4. Afficher le montant du compte (cf. étape 2)
 5. Retirer 125.25 sesterces sur le compte en appelant la méthode `withdraw`
 6. Afficher le montant du compte (cf. étape 2)
 5. Retirer 1337.0 sesterces sur le compte en appelant la méthode `withdraw` (ceci ne devrait avoir aucun effet)
 6. Afficher le montant du compte (cf. étape 2)


## Partie 2 : Développer à partir d'un cahier des charges fonctionnel

Cette partie teste la capacité à modéliser des classes et à les utiliser à partir d'une description non technique. Dans cette partie, ils faudra
 1. Identifier les éléments du cahier des charges qui méritent la création d'une classe, et leurs relation avec les autres éléments (héritage, ...)
 2. Déterminer les informations qui caractérisent ces concepts pour en faire des attributs
 2. Déterminer les actions possibles sur ces concepts et en faire des méthodes ou spécialiser des méthodes (override)
 
Le code de cette partie prend comme point de départ le code de la partie 1. N'oubliez donc pas de créer un tag git sur le commit final de la partie 1 (demandez de l'aide si nécéssaire pour la création d'un tag git)

### Cahier des charges 

#### Compte courant et compte épargne

Les clients doivent pouvoir ouvrir deux types de comptes différents : un compte courant ou un compte épargne.

 * Les comptes courants : 
   * autorisent un montant négatif jusqu'a -1000.0 (il est possible de retirer de l'argent tant que le solde resterait supérieur à -1000)
   * Ne génèrent pas d'intérèts
 * Les comptes épargnes : 
   * ont un taux d'intérêts déterminé à la création
   * Ne peuvent jamais être vides (le montant doit toujours être supérieur à 0, y compris à la création)
   * Doivent permettre de créditer des intérêts : lors du crédit des intérêts, le montant du compte doit être augmenté en fonction du taux d'intérêts.
    
    
#### Scénario d'exemple

Pour tester les classes développées dans cette partie, voilà un scénario d'exemple qui peut être développé dans la méthode main (ou dans un test unitaire...)

 2. Ouvrir un compte courant à obélix en sesterces
 3. ouvrir un compte épargne avec un taux d'intérêts de 0.01 pour obélix en dollars en y déposant 1$ comme somme initiale
 4. Déposer 100 sesterces sur le compte courant, et 30$ sur le compte épargne
 5. Retirer 200 sesterces du compte courant et afficher le montant dispo sur ce compte
 6. Créditer les intérêts sur le compte épargne et afficher le montant dispo sur ce compte.
 
## Bonus
 
Les objectifs suivants sont facultatifs. Ils seront valorisés s'ils sont présents mais n'entrainent aucune pénalité s'ils sont absent. 

#### Gestion des clients

Pour des raisons légales, le nom des clients qui possèdent un compte ne peut pas toujours être utilisé. Un client doit donc pouvoir être idéntifié par un numéro de client. Le numéro de client est un simple nombre entier généré aléatoirement.

 à la création d'un client, on stocke son nom et on mémorise son numéro de client.
 
 Une fois le client créé, son numéro client ne peut plus changer mais son nom peut être mis à jour. 
 
 Dans un compte bancaire, on ne stocke plus le nom du client mais son numéro de client.

à partir d'un client, il doit être possible d'afficher la liste de ses comptes bancaires et leurs montants.

#### Retrait impossible

Au lieu de ne rien faire lors d'un retrait impossible, la méthode `withdraw` de la classe `Account` pourrait renvoyer une erreur.

Utiliser une exception pour signaler à la méthode appelante que le retrait est impossible.

#### MonetaryAmount immutable

Il est possible que votre code présente une faille dans la méthode `getCurrentBalance` de la classe `Account`. La méthode retourne l'instance de MonetaryAccount qui est stockée dans son attribut `balance`. L'instance retournée peut alors être modifiée, ce qui change la quantité d'argent disponible sans passer par les méthodes `deposit` et `withdraw`.

 Résoudre le problème en rendant la classe MonetaryAccount immutable.
 
#### Ouverture / fermeture de comptes

On souhaite ajouter une fonctionnalité d'ouverture / fermeture de compte.

Pour un client donné, il doit être possible d'ouvrir et de fermer des comptes.

 * Lors de l'ouverture de compte, un compte bancaire est ajouté à la liste des comptes du client. Il doit être possible d'ouvrir les deux types de comptes.
 * Un client possède un et un seul compte courant
 * Quand un client ferme un compte, l'ensemble de l'argent présent sur ce compte est recrédité sur le compte courant du client. Pensez à la conversion entre devises (il faudra une table de conversion quelque part...)